post-setup.sh is my post install script to set the system up to my liking.

to install run the following: -

.. code-block:: bash

  sh -c "$(curl -fsSL https://bitbucket.org/darrenbaker/antergos-post-install/raw/master/post-setup.sh)"

  or

  wget https://bit.ly/2G6fVLe # only the post-setup.sh script
