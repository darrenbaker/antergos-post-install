#!/bin/bash
#
# (c)2018 MAD
# Antergos minimal install --> My desktop
#
# after install from ISO run this script

LOG="post-install.log"

## functions

# update packages
refresh_packages() {
    sudo pacman -Sqyy
}

# remove any packages here
remove_package() {
    echo -n "Removing ${*}"
    sudo pacman -R --noconfirm --noprogressbar ${*} 2>>$LOG 1>&2 && echo " - OK" || echo " - FAIL"
}

install_package() {
    echo -n "Installing ${*}"
    sudo pacman -Sq --noconfirm --noprogressbar ${*} 2>>$LOG 1>&2 && echo " - OK" || echo " - FAIL"
}

build_package() {
    echo -n "Building ${*}"
    yaourt -S --noconfirm ${*} 2>>$LOG 1>&2 && echo " - OK" || echo " - FAIL"
}

# clean log
>$LOG


## REMOVE ANY PACKAGES HERE

## BASE INSTALL (needed files, install java early to avoid other packages installing a different version))

install_package jre8-openjdk java-openjfx
#install_package jre8-openjdk java-openjfx
#build_package google-chrome
build_package google-chrome-beta
#build_package google-chrome-dev
install_package arc-solid-gtk-theme
install_package xcursor-comix
#build_package paper-icon-theme-git # this can take a while to build
install_package papirus-icon-theme
install_package noto-fonts
install_package ttf-hack
install_package otf-fira-code
install_package vim vim-runtime ctags
install_package htop screenfetch ccze aspell-en exim

## FULL INSTALL (not needed in a virtual environment)
install_package speedtest-cli
install_package soundconverter audacity handbrake youtube-dl
install_package meld filezilla python-sqlparse
install_package gitkraken
#build_package smartgit
install_package android-tools android-udev
install_package nvidia-installer && sudo nvidia-installer -b
build_package visual-studio-code-bin

## OPTIONAL INSTALLS
#install_package vagrant
#install_package virtualbox virtualbox-host-dkms
#install_package libreoffice-fresh libreoffice-fresh-en-GB
#install_package wine lib32-libldap lib32-gnutls lib32-alsa-plugins lib32-libpulse lib32-openal lib32-mpg123
#install_package labyrinth
#install_package go go-tools


## SET SOME SETTINGS AND PREPARE THE DESKTOP
# apply themes
#gsettings set org.gnome.desktop.interface icon-theme 'Paper'
gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'
gsettings set org.gnome.desktop.interface cursor-theme 'ComixCursors-White'
gsettings set org.gnome.desktop.interface gtk-theme 'Arc-Dark-solid'
gsettings set org.gnome.shell.extensions.user-theme name 'Numix-Frost'

# set the clock to 24h
gsettings set org.gnome.desktop.interface clock-format '24h'

# dock settings
dconf write /org/gnome/shell/extensions/dash-to-dock/dock-position "'BOTTOM'"
dconf write /org/gnome/shell/extensions/dash-to-dock/custom-theme-shrink true
dconf write /org/gnome/shell/extensions/dash-to-dock/custom-theme-running-dots true

# terminal settings
dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/font "'Hack 12'"
dconf write /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/use-system-font false

# gedit settings
gsettings set org.gnome.gedit.preferences.editor scheme 'oblivion'
gsettings set org.gnome.gedit.preferences.editor editor-font 'Hack 12'
gsettings set org.gnome.gedit.preferences.editor use-default-font false
gsettings set org.gnome.gedit.preferences.editor bracket-matching true
gsettings set org.gnome.gedit.preferences.editor insert-spaces true
gsettings set org.gnome.gedit.preferences.editor highlight-current-line true
gsettings set org.gnome.gedit.preferences.editor auto-indent true

# set the favourites bar
gsettings set org.gnome.shell favorite-apps "['google-chrome-beta.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Terminal.desktop']"

# set the background
#curl -fsSL https://bitbucket.org/darrenbaker/antergos-post-install/raw/master/background.png > $HOME/Pictures/background.png
curl -fsSL https://bitbucket.org/darrenbaker/antergos-post-install/raw/master/background1.png > $HOME/Pictures/background.png
gsettings set org.gnome.desktop.background picture-uri file://$HOME/Pictures/background.png

# set nautils options
gsettings set org.gnome.nautilus.preferences automatic-decompression false

# set desktop interface fonts
gsettings set org.gnome.desktop.interface font-name 'Noto Sans 11'
gsettings set org.gnome.desktop.interface document-font-name 'Noto Sans 11'
gsettings set org.gnome.desktop.interface monospace-font-name 'Hack 12'
gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Noto Sans Bold 11'

# setup topbar settings
gsettings set org.gnome.desktop.interface show-battery-percentage true
gsettings set org.gnome.desktop.calendar show-weekdate true

# clean pacman and make sure everything is up to date
sudo pacman -Scc
sudo pacman -Su --noconfirm

# add user to some groups
sudo usermod -aG adbusers darren

# cleanup
# remove the wine mime types arrrghhhhh!!!
find ~/.local/share | grep wine | xargs rm
update-desktop-database ~/.local/share/applications
update-mime-database ~/.local/share/mime/

# whats left
echo '.'
echo '.'
echo 'TODO:'
echo '- use Gitkraken to get the scripts repo and link the bin'
echo '- run stup_bin'
echo '- run setup_vscode'
echo '- install gnome extension TopIcons Plus for the system tray icons to be displayed'
echo '.'
echo '.'
echo '- Reboot to make sure all changes are picked up!!'
echo '.'
echo 'Have fun...'

